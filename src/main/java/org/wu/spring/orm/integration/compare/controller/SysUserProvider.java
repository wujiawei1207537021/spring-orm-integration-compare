package org.wu.spring.orm.integration.compare.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.core.NormalUsedString;
import org.wu.framework.core.utils.DataTransformUntil;
import org.wu.framework.easy.excel.endpoint.EasyExcelFieldPoint;
import org.wu.framework.easy.excel.endpoint.ExportFieldCommand;
import org.wu.framework.easy.excel.service.style.DefaultStyleParam;
import org.wu.framework.easy.excel.service.style.Style;
import org.wu.framework.easy.excel.service.style.StyleParam;
import org.wu.framework.easy.excel.stereotype.EasyExcel;
import org.wu.framework.easy.excel.stereotype.EasyFile;
import org.wu.framework.easy.excel.toolkit.DynamicEasyExcelContextHolder;
import org.wu.framework.easy.excel.util.ExportFieldCommandUtils;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.wu.framework.web.spring.EasyController;
import org.wu.spring.orm.integration.compare.application.SysUserApplication;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserQueryListCommand;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserQueryOneCommand;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserRemoveCommand;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserStoryCommand;
import org.wu.spring.orm.integration.compare.application.dto.SysUserDTO;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUser;
import org.wu.spring.orm.integration.compare.infrastructure.entity.OrmCompareRecordsDO;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * describe sys_user
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyController
 **/
@Slf4j
@Tag(name = "ORM 操作用户信息统计提供者")
@EasyController("/sys/user")
public class SysUserProvider {

    @Resource
    private SysUserApplication sysUserApplication;

    @Resource
    private LazyLambdaStream lazyLambdaStream;

    /**
     * describe 新增
     *
     * @return {@link Result <SysUser>} 新增后领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Operation(summary = "新增、修改")
    @PostMapping("/story")
    public Result<SysUser> story() {
        // 模拟生成数据
        return sysUserApplication.story(DataTransformUntil.simulationBean(SysUserStoryCommand.class));
    }

    /**
     * describe 批量新增
     *
     * @param size 批量新增   数量
     * @return {@link Result<List<SysUser>>} 新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Operation(summary = "批量新增")
    @PostMapping("/batchStory")
    public Result<List<SysUser>> batchStory(@RequestParam(value = "size", defaultValue = "10") int size) {
        List<SysUserStoryCommand> sysUserStoryCommands = DataTransformUntil.simulationBeanList(SysUserStoryCommand.class, size);
        return sysUserApplication.batchStory(sysUserStoryCommands);
    }

    /**
     * describe 查询单个
     *
     * @param sysUserQueryOneCommand 查询单个
     * @return {@link Result<SysUserDTO>} DTO对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Operation(summary = "查询单个")
    @GetMapping("/findOne")
    public Result<SysUserDTO> findOne(@ModelAttribute SysUserQueryOneCommand sysUserQueryOneCommand) {
        return sysUserApplication.findOne(sysUserQueryOneCommand);
    }

    /**
     * describe 查询多个
     *
     * @param sysUserQueryListCommand 查询多个
     * @return {@link Result<List<SysUserDTO>>} DTO对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Operation(summary = "查询多个")
    @GetMapping("/findList")
    public Result<List<SysUserDTO>> findList(@ModelAttribute SysUserQueryListCommand sysUserQueryListCommand) {
        return sysUserApplication.findList(sysUserQueryListCommand);
    }

    /**
     * describe 分页查询多个
     *
     * @param sysUserQueryListCommand 分页查询多个
     * @return {@link Result<LazyPage<SysUserDTO>>} 分页DTO对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Operation(summary = "分页查询多个")
    @GetMapping("/findPage")
    public Result<LazyPage<SysUserDTO>> findPage(@Parameter(description = "分页大小") @RequestParam(defaultValue = "10", value = "size") int size,
                                                 @Parameter(description = "当前页数") @RequestParam(defaultValue = "1", value = "current") int current, @ModelAttribute SysUserQueryListCommand sysUserQueryListCommand) {
        return sysUserApplication.findPage(size, current, sysUserQueryListCommand);
    }

    /**
     * describe 删除
     *
     * @param sysUserRemoveCommand 删除
     * @return {@link Result<SysUser>}
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Operation(summary = "删除")
    @DeleteMapping("/remove")
    public Result<SysUser> remove(@ModelAttribute SysUserRemoveCommand sysUserRemoveCommand) {
        return sysUserApplication.remove(sysUserRemoveCommand);
    }

    /**
     * 一轮比对
     */
    @Transactional(rollbackFor = Exception.class)
    @Operation(summary = "一轮比对")
    @GetMapping("/oneRound")
    public void oneRound(@RequestParam(value = "size",defaultValue = "1") int size) {
        this.batchStory(size);
        this.findPage(size, 1, new SysUserQueryListCommand());

    }

    /**
     * 进行数据细节比较
     */
    @Operation(summary = "进行数据细节比较")
    @GetMapping("/run-particulars-compare")
    public void runParticularsCompare() {
        lazyLambdaStream.delete(LazyWrappers.<SysUserDO>lambdaWrapper().notNull(SysUserDO::getId));
        lazyLambdaStream.delete(LazyWrappers.<OrmCompareRecordsDO>lambdaWrapper().notNull(OrmCompareRecordsDO::getId));

        // 100000
        int totalSize = 20000;
        /**
         * 一轮比对
         */
        for (int i = 1; i < totalSize; i++) {
            this.oneRound(i);
        }
        log.info("进行数据细节比较 success");
    }
    /**
     * 从一万开始进行数据细节比较
     */
    @Operation(summary = "从一万开始进行数据细节比较")
    @GetMapping("/run-particulars-compare-from-10000")
    public void runParticularsCompareFrom10000() {
        lazyLambdaStream.delete(LazyWrappers.<SysUserDO>lambdaWrapper().notNull(SysUserDO::getId));
        lazyLambdaStream.delete(LazyWrappers.<OrmCompareRecordsDO>lambdaWrapper().notNull(OrmCompareRecordsDO::getId));

        // 100000
        int totalSize = 20000;
        /**
         * 一轮比对
         */
        for (int i = 10000; i < totalSize; i++) {
            this.oneRound(i);
        }
        log.info("从一万开始进行数据细节比较 success");
    }

    /**
     * 快速进行数据比较
     */
    @Operation(summary = "快速进行数据比较")
    @GetMapping("/run-compare")
    public Result runCompare() {
//        Connection connection = DataSourceUtils.getConnection(dataSource);
//
//        DataSourceUtils.releaseConnection();
//         DataSourceUtils.isConnectionTransactional(con, dataSource);
//        StringUtils.startsWithIgnoreCase()
//        AnnotatedElementUtils.findMergedAnnotation()
//        AnnotatedElementUtils.getMergedAnnotation()


        // 删除就数据
        lazyLambdaStream.delete(LazyWrappers.<SysUserDO>lambdaWrapper().notNull(SysUserDO::getId));
        lazyLambdaStream.delete(LazyWrappers.<OrmCompareRecordsDO>lambdaWrapper().notNull(OrmCompareRecordsDO::getId));

        // 10 的幂数
        int power = 5;
        // 执行批量插入
        int size = 10;
        for (int i = 0; i < power; i++) {
            this.oneRound(size);
            size = size * 10;
        }
        log.info("快速进行数据比较 success");
        return ResultFactory.successOf();
    }

    /**
     * 进行数据比较
     */
    @EasyFile(fileType = EasyFile.FileType.STRING_TYPE, suffix = "md", fileName = "比较mybatis、lazy、sqltoy、mybatis-flex操作数据")
    @Operation(summary = "下载比较数据(MarkDown)")
    @GetMapping("/export-compare-result/markdown")
    public String exportCompareResultMarkdown() {

        // 查询结果
        List<OrmCompareRecordsDO> ormCompareRecordsDOList = lazyLambdaStream
                .selectList(LazyWrappers.<OrmCompareRecordsDO>lambdaWrapper().notNull(OrmCompareRecordsDO::getId)
//                        .ignoreAs(OrmCompareRecordsDO::getOperationTime)
//                        .functionAs(" avg(operation_time) ",OrmCompareRecordsDO::getOperationTime)
                                .orderByAsc(OrmCompareRecordsDO::getAffectsRows)
                                .orderByAsc(OrmCompareRecordsDO::getOrm)
//                        .groupBy(OrmCompareRecordsDO::getOrm,OrmCompareRecordsDO::getType)
                );

        Map<String, Map<String, List<OrmCompareRecordsDO>>> collect = ormCompareRecordsDOList.stream()
                .collect(Collectors.groupingBy(OrmCompareRecordsDO::getType, Collectors.groupingBy(OrmCompareRecordsDO::getOrm)));


        // 转换成 md 文件 简略
        StringBuilder mdStringBuilder = new StringBuilder();

        collect.forEach((type, ormCompareRecordsDOS) -> {

            ormCompareRecordsDOS.forEach((orm, ormCompareRecordsDOList1) -> {
                //
                mdStringBuilder.append(NormalUsedString.PIPE);
                mdStringBuilder.append(orm).append("(").append(type).append(")");
                mdStringBuilder.append(NormalUsedString.PIPE);
                for (OrmCompareRecordsDO ormCompareRecordsDO : ormCompareRecordsDOList1) {
                    mdStringBuilder.append("影响行数:").append(ormCompareRecordsDO.getAffectsRows());
                    mdStringBuilder.append(NormalUsedString.PIPE);
                }
                mdStringBuilder.append(NormalUsedString.NEWLINE);


                mdStringBuilder.append(NormalUsedString.PIPE);
                for (int i = 0; i <= ormCompareRecordsDOList1.size(); i++) {
                    mdStringBuilder.append("---" + NormalUsedString.PIPE);
                }
                mdStringBuilder.append(NormalUsedString.NEWLINE);

                // 填充数据
                mdStringBuilder.append(NormalUsedString.PIPE);
                mdStringBuilder.append("执行时间:");
                mdStringBuilder.append(NormalUsedString.PIPE);
                for (OrmCompareRecordsDO ormCompareRecordsDO : ormCompareRecordsDOList1) {
                    mdStringBuilder.append("**");
                    mdStringBuilder.append(ormCompareRecordsDO.getOperationTime()).append("毫秒");
                    mdStringBuilder.append("**");
                    mdStringBuilder.append(NormalUsedString.PIPE);
                }
                mdStringBuilder.append(NormalUsedString.NEWLINE);


                // 填充数据
//                mdStringBuilder.append(NormalUsedString.PIPE);
//                mdStringBuilder.append("执行结果:");
//                mdStringBuilder.append(NormalUsedString.PIPE);
//
//                for (OrmCompareRecordsDO ormCompareRecordsDO : ormCompareRecordsDOList1) {
//                    mdStringBuilder.append(ormCompareRecordsDO.isSuccess());
//                    mdStringBuilder.append(NormalUsedString.PIPE);
//                }
                mdStringBuilder.append(NormalUsedString.NEWLINE);
                mdStringBuilder.append(NormalUsedString.NEWLINE);
                mdStringBuilder.append(NormalUsedString.NEWLINE);
                mdStringBuilder.append(NormalUsedString.NEWLINE);
            });


        });


        return mdStringBuilder.toString();
    }

    /**
     * 进行数据比较
     * <per>
     * <code></code>
     * <json>
     * [
     * [
     * {
     * "data": [
     * {
     * "100": 16,
     * "1000": 94,
     * "10000": 733,
     * "orm": "MYBATIS_FLEX",
     * "10": 30
     * },
     * {
     * "100": 6,
     * "1000": 37,
     * "10000": 247,
     * "orm": "WOOD",
     * "10": 19
     * },
     * {
     * "100": 15,
     * "1000": 88,
     * "10000": 589,
     * "orm": "MYBATIS_MP",
     * "10": 27
     * },
     * {
     * "100": 38,
     * "1000": 107,
     * "10000": 747,
     * "orm": "MYBATIS_PLUS",
     * "10": 67
     * },
     * {
     * "100": 16,
     * "1000": 90,
     * "10000": 764,
     * "orm": "SQLTOY",
     * "10": 41
     * },
     * {
     * "100": 25,
     * "1000": 177,
     * "10000": 2853,
     * "orm": "LAZY",
     * "10": 155
     * },
     * {
     * "100": 9,
     * "100000": 4348,
     * "1000": 48,
     * "10000": 416,
     * "orm": "JDBC",
     * "10": 9
     * },
     * {
     * "100": 21,
     * "100000": 6706,
     * "1000": 111,
     * "10000": 700,
     * "orm": "DB_VISITOR",
     * "10": 26
     * },
     * {
     * "100": 596,
     * "1000": 30910,
     * "10000": 310022,
     * "orm": "JPA",
     * "10": 117
     * },
     * {
     * "100": 21,
     * "100000": 6127,
     * "1000": 91,
     * "10000": 587,
     * "orm": "EASY_QUERY",
     * "10": 52
     * },
     * {
     * "100": 14,
     * "100000": 5138,
     * "1000": 62,
     * "10000": 489,
     * "orm": "HAMMER_SQL_DB",
     * "10": 41
     * },
     * {
     * "100": 27,
     * "100000": 9234,
     * "1000": 105,
     * "10000": 1051,
     * "orm": "DREAM_ORM",
     * "10": 60
     * },
     * {
     * "100": 35,
     * "100000": 5105,
     * "1000": 102,
     * "10000": 540,
     * "orm": "BEETL_SQL",
     * "10": 216
     * }
     * ],
     * "type": "batchStory"
     * }
     * ],
     * [
     * {
     * "data": [
     * {
     * "100": 16,
     * "1000": 55,
     * "10000": 426,
     * "orm": "MYBATIS_FLEX",
     * "10": 33
     * },
     * {
     * "100": 5,
     * "1000": 18,
     * "10000": 131,
     * "orm": "WOOD",
     * "10": 8
     * },
     * {
     * "100": 10,
     * "1000": 37,
     * "10000": 245,
     * "orm": "MYBATIS_MP",
     * "10": 25
     * },
     * {
     * "100": 21,
     * "1000": 51,
     * "10000": 350,
     * "orm": "MYBATIS_PLUS",
     * "10": 77
     * },
     * {
     * "100": 6,
     * "1000": 31,
     * "10000": 137,
     * "orm": "SQLTOY",
     * "10": 37
     * },
     * {
     * "100": 8,
     * "1000": 26,
     * "10000": 170,
     * "orm": "LAZY",
     * "10": 21
     * },
     * {
     * "100": 6,
     * "1000": 37,
     * "10000": 261,
     * "orm": "JDBC",
     * "10": 3
     * },
     * {
     * "100": 7,
     * "1000": 45,
     * "10000": 290,
     * "orm": "DB_VISITOR",
     * "10": 23
     * },
     * {
     * "100": 20,
     * "1000": 78,
     * "10000": 246,
     * "orm": "JPA",
     * "10": 109
     * },
     * {
     * "100": 6,
     * "1000": 45,
     * "10000": 155,
     * "orm": "EASY_QUERY",
     * "10": 84
     * },
     * {
     * "100": 13,
     * "1000": 31,
     * "10000": 198,
     * "orm": "HAMMER_SQL_DB",
     * "10": 105
     * },
     * {
     * "100": 13,
     * "1000": 51,
     * "10000": 168,
     * "orm": "DREAM_ORM",
     * "10": 119
     * },
     * {
     * "100": 13,
     * "1000": 82,
     * "10000": 256,
     * "orm": "BEETL_SQL",
     * "10": 58
     * }
     * ],
     * "type": "findPage"
     * }
     * ]
     * ]
     * </json>
     * </per>
     */
    @EasyExcel(fileName = "比较mybatis、lazy、sqltoy、mybatis-flex操作数据", useAnnotation = false, style = DefaultStyle.class)
    @Operation(summary = "下载比较数据(Excel)")
    @GetMapping("/export-compare-result/excel")
    public List<Map<String, Object>> exportCompareResultExcel() {

        // 查询结果
        List<OrmCompareRecordsDO> ormCompareRecordsDOList = lazyLambdaStream
                .selectList(LazyWrappers.<OrmCompareRecordsDO>lambdaWrapper().notNull(OrmCompareRecordsDO::getId)
                        .orderByAsc(OrmCompareRecordsDO::getAffectsRows)
                        .orderByAsc(OrmCompareRecordsDO::getOrm)
                );
        // 查询影响行数作为表头
        List<OrmCompareRecordsDO> ormCompareRecordsDOS = lazyLambdaStream.selectList(LazyWrappers.<OrmCompareRecordsDO>lambdaWrapper()
                .groupBy(OrmCompareRecordsDO::getAffectsRows)
        );

        // 需要导出的数据设置信息
        List<ExportFieldCommand> exportFieldCommands = new ArrayList<>();
        exportFieldCommands.add(new ExportFieldCommand("type", "执行类型"));
        exportFieldCommands.add(new ExportFieldCommand("data.$orm", "框架"));
        for (OrmCompareRecordsDO ormCompareRecordsDO : ormCompareRecordsDOS) {
            exportFieldCommands.add(new ExportFieldCommand("data.$" + ormCompareRecordsDO.getAffectsRows(), "影响数据" + ormCompareRecordsDO.getAffectsRows() + "行/执行时间"));
        }


        List<EasyExcelFieldPoint> EasyExcelFieldPointList = ExportFieldCommandUtils.exportFieldCommandList2EasyExcelFiledPointList(exportFieldCommands);

        DynamicEasyExcelContextHolder.pushOnlyExportField(EasyExcelFieldPointList);

        Map<String, Map<String, List<OrmCompareRecordsDO>>> mapMap = ormCompareRecordsDOList.stream()
                .collect(Collectors.groupingBy(OrmCompareRecordsDO::getType, Collectors.groupingBy(OrmCompareRecordsDO::getOrm)));

        return mapMap.entrySet().stream().map(typeStringMapEntry -> {
            String type = typeStringMapEntry.getKey();
            Map<String, List<OrmCompareRecordsDO>> typeStringMapEntryValue = typeStringMapEntry.getValue();
            Map<String, Object> typeItem = new HashMap<>();
            typeItem.put("type", type);
            // 根据type 区分 sheet
            List<Map<String, Object>> ormItemList = typeStringMapEntryValue.entrySet().stream().map(ormStringListEntry -> {
                String orm = ormStringListEntry.getKey();
                List<OrmCompareRecordsDO> compareRecordsDOList = ormStringListEntry.getValue();
                Map<String, Object> ormItem =
                        compareRecordsDOList
                                .stream()
                                .collect(
                                        Collectors.toMap(ormCompareRecordsDO -> ormCompareRecordsDO.getAffectsRows().toString(), OrmCompareRecordsDO::getOperationTime)
                                );
                ormItem.put("orm", orm);
                return ormItem;
            }).toList();

            typeItem.put("data", ormItemList);
            return typeItem;
        }).toList();
    }
}


/**
 * 自定义导出样式
 */
class DefaultStyle implements Style {
    public DefaultStyle() {
    }

    /**
     * @deprecated
     */
    @Deprecated
    public CellStyle titleStyle(StyleParam styleParam) {
        CellStyle style = styleParam.getWorkbook().createCellStyle();
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setAlignment(HorizontalAlignment.CENTER);
        Font font = styleParam.getWorkbook().createFont();
        style.setFont(font);
        return style;
    }

    public CellStyle titleStyle(DefaultStyleParam styleParam) {

        CellStyle style = styleParam.getWorkbook().createCellStyle();
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setAlignment(HorizontalAlignment.CENTER);
        Font font = styleParam.getWorkbook().createFont();
        style.setFont(font);
        return style;
    }

    public CellStyle columnStyle(StyleParam styleParam) {
        CellStyle style = styleParam.getWorkbook().createCellStyle();
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        Font font = styleParam.getWorkbook().createFont();
        style.setFont(font);
        style.setFont(font);
        return style;
    }

    public CellStyle columnStyle(DefaultStyleParam styleParam) {
        CellStyle style = styleParam.getWorkbook().createCellStyle();
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        Font font = styleParam.getWorkbook().createFont();
        style.setFont(font);
        style.setFont(font);
        return style;
    }
}
