package org.wu.spring.orm.integration.compare.config;

import org.beetl.sql.core.SQLSource;
import org.beetl.sql.core.db.AbstractDBStyle;
import org.beetl.sql.core.db.MySqlStyle;
import org.beetl.sql.core.range.RangeSql;

public class BeetlSqlInsertIgnoreMysqlType extends MySqlStyle {
    @Override
    protected SQLSource generalInsert(Class<?> cls, boolean template) {
        SQLSource sqlSource = super.generalInsert(cls, template);
        String upsert = sqlSource.template.replaceFirst("insert into", "insert ignore into");
        sqlSource.template = upsert;
        return sqlSource;
    }
}
