package org.wu.spring.orm.integration.compare.infrastructure.persistence.dream;

import com.dream.template.annotation.Conditional;
import com.dream.template.condition.ContainsCondition;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SysUserCondition {


    /**
     * 额外字段
     */
    @Conditional(value = ContainsCondition.class)
    @Schema(description = "额外字段", name = "columnName", example = "")
    private String columnName;

    /**
     * 创建时间
     */
    @Conditional(value = ContainsCondition.class)
    @Schema(description = "创建时间", name = "createTime", example = "")
    private LocalDateTime createTime;

    /**
     * 用户ID
     */
    @Conditional(value = ContainsCondition.class)
    @Schema(description = "用户ID", name = "id", example = "")
    private Long id;

    /**
     * null
     */
    @Conditional(value = ContainsCondition.class)
    @Schema(description = "null", name = "isDeleted", example = "")
    private Boolean isDeleted;

    /**
     * 密码
     */
    @Conditional(value = ContainsCondition.class)
    @Schema(description = "密码", name = "password", example = "")
    private String password;

    /**
     * null
     */
    @Conditional(value = ContainsCondition.class)
    @Schema(description = "null", name = "scope", example = "")
    private String scope;

    /**
     * 状态
     */
    @Conditional(value = ContainsCondition.class)
    @Schema(description = "状态", name = "status", example = "")
    private Boolean status;


    /**
     * 用户名
     */
    @Conditional(value = ContainsCondition.class)
    @Schema(description = "用户名", name = "username", example = "")
    private String username;


}
