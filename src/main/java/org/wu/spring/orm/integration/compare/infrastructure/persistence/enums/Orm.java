package org.wu.spring.orm.integration.compare.infrastructure.persistence.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Orm {
    LAZY,
    MYBATIS_PLUS,
    SQLTOY,
    MYBATIS_FLEX,
    EASY_QUERY,
    MYBATIS_MP,
    JPA,
    DB_VISITOR,
    BEETL_SQL,
    DREAM_ORM,
    WOOD,
    HAMMER_SQL_DB,
    JDBC,
    QDBC,
}
