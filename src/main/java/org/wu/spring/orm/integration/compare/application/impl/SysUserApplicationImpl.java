package org.wu.spring.orm.integration.compare.application.impl;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.spring.orm.integration.compare.application.SysUserApplication;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUser;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserRemoveCommand;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserStoryCommand;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserUpdateCommand;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserQueryListCommand;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserQueryOneCommand;
import org.wu.spring.orm.integration.compare.application.assembler.SysUserDTOAssembler;
import org.wu.spring.orm.integration.compare.application.dto.SysUserDTO;

import org.wu.framework.database.lazy.web.plus.stereotype.LazyApplication;
import java.util.stream.Collectors;

import jakarta.annotation.Resource;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUserRepository;

import java.util.List;


/**
 * describe sys_user
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 **/
@LazyApplication
public class SysUserApplicationImpl implements SysUserApplication {

    @Resource
    SysUserRepository sysUserRepository;

    /**
     * describe 新增
     *
     * @param sysUserStoryCommand 新增
     * @return {@link Result <SysUser>} 新增后领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> story(SysUserStoryCommand sysUserStoryCommand) {
        SysUser sysUser = SysUserDTOAssembler.INSTANCE.toSysUser(sysUserStoryCommand);
        try {
            return sysUserRepository.story(sysUser);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * describe 批量新增
     *
     * @param sysUserStoryCommandList 批量新增
     * @return {@link Result<List<SysUser>>} 新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> batchStory(List<SysUserStoryCommand> sysUserStoryCommandList) {
        List<SysUser> sysUserList = sysUserStoryCommandList.stream().map(SysUserDTOAssembler.INSTANCE::toSysUser).collect(Collectors.toList());
        try {
            return sysUserRepository.batchStory(sysUserList);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * describe 更新
     *
     * @param sysUserUpdateCommand 更新
     * @return {@link Result<SysUser>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> updateOne(SysUserUpdateCommand sysUserUpdateCommand) {
        SysUser sysUser = SysUserDTOAssembler.INSTANCE.toSysUser(sysUserUpdateCommand);
        try {
            return sysUserRepository.story(sysUser);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * describe 查询单个
     *
     * @param sysUserQueryOneCommand 查询单个
     * @return {@link Result<SysUserDTO>} DTO对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUserDTO> findOne(SysUserQueryOneCommand sysUserQueryOneCommand) {
        SysUser sysUser = SysUserDTOAssembler.INSTANCE.toSysUser(sysUserQueryOneCommand);
        try {
            return sysUserRepository.findOne(sysUser).convert(SysUserDTOAssembler.INSTANCE::fromSysUser);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * describe 查询多个
     *
     * @param sysUserQueryListCommand 查询多个
     * @return {@link Result<List<SysUserDTO>>} DTO对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUserDTO>> findList(SysUserQueryListCommand sysUserQueryListCommand) {
        SysUser sysUser = SysUserDTOAssembler.INSTANCE.toSysUser(sysUserQueryListCommand);
        try {
            return sysUserRepository.findList(sysUser).convert(sysUsers -> sysUsers.stream().map(SysUserDTOAssembler.INSTANCE::fromSysUser).collect(Collectors.toList()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * describe 分页查询多个
     *
     * @param sysUserQueryListCommand 分页查询多个
     * @return {@link Result<LazyPage<SysUserDTO>>} 分页DTO对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<LazyPage<SysUserDTO>> findPage(int size, int current, SysUserQueryListCommand sysUserQueryListCommand) {
        SysUser sysUser = SysUserDTOAssembler.INSTANCE.toSysUser(sysUserQueryListCommand);
        try {
            return sysUserRepository.findPage(size, current, sysUser).convert(page -> page.convert(SysUserDTOAssembler.INSTANCE::fromSysUser));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * describe 删除
     *
     * @param sysUserRemoveCommand 删除
     * @return {@link Result<SysUser>}
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> remove(SysUserRemoveCommand sysUserRemoveCommand) {
        SysUser sysUser = SysUserDTOAssembler.INSTANCE.toSysUser(sysUserRemoveCommand);
        try {
            return sysUserRepository.remove(sysUser);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}