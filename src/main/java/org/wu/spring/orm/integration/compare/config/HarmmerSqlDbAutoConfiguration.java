package org.wu.spring.orm.integration.compare.config;


import cn.featherfly.common.db.dialect.Dialects;
import cn.featherfly.common.db.mapping.JdbcMappingFactory;
import cn.featherfly.common.db.mapping.JdbcMappingFactoryImpl;
import cn.featherfly.common.db.metadata.DatabaseMetadata;
import cn.featherfly.common.db.metadata.DatabaseMetadataManager;
import cn.featherfly.constant.ConstantConfigurator;
import cn.featherfly.hammer.config.HammerConfigImpl;
import cn.featherfly.hammer.sqldb.SqldbHammer;
import cn.featherfly.hammer.sqldb.SqldbHammerImpl;
import cn.featherfly.hammer.sqldb.jdbc.Jdbc;
import cn.featherfly.hammer.sqldb.jdbc.JdbcSpringImpl;
import cn.featherfly.hammer.tpl.TplConfigFactory;
import cn.featherfly.hammer.tpl.TplConfigFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.wu.framework.lazy.orm.database.lambda.LazyOperation;
import org.wu.spring.orm.integration.compare.infrastructure.entity.OrmCompareRecordsDO;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;

import javax.sql.DataSource;

@Configuration
public class HarmmerSqlDbAutoConfiguration {


    private static final Logger log = LoggerFactory.getLogger(HarmmerSqlDbAutoConfiguration.class);

    @Bean
    public SqldbHammer hammer(DataSource dataSource,LazyOperation lazyOperation) {
        lazyOperation.createTable(OrmCompareRecordsDO.class);
        lazyOperation.createTable( SysUserDO.class);
        DatabaseMetadata metadata = DatabaseMetadataManager.getDefaultManager().create(dataSource);
        Jdbc jdbc = new JdbcSpringImpl(dataSource, Dialects.MYSQL,metadata);
        JdbcMappingFactory mappingFactory = new JdbcMappingFactoryImpl(metadata, Dialects.MYSQL);
        // tpl/代表sql模板从classpath查找的根目录，如果调用无参构造函数，则从classpath目录开始查找
        TplConfigFactory configFactory = new TplConfigFactoryImpl();
        HammerConfigImpl hammerConfig = new HammerConfigImpl();
        SqldbHammerImpl hammer = new SqldbHammerImpl(jdbc, mappingFactory, configFactory,hammerConfig);
        return hammer;
    }

}
