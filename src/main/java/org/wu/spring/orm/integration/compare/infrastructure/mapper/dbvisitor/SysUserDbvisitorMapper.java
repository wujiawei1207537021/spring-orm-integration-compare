package org.wu.spring.orm.integration.compare.infrastructure.mapper.dbvisitor;

import net.hasor.dbvisitor.dal.mapper.BaseMapper;
import net.hasor.dbvisitor.dal.repository.Param;
import net.hasor.dbvisitor.dal.repository.RefMapper;

import net.hasor.dbvisitor.dal.repository.SimpleMapper;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;

import java.util.List;

@SimpleMapper
@RefMapper("/dbvisitor/SysUserDbvisitorMapper.xml")
public interface SysUserDbvisitorMapper extends BaseMapper<SysUserDO> {

    void story(@Param("item") SysUserDO sysUserDO);

    void batchStory(@Param("list") List<SysUserDO> sysUserDOList);
}
