package org.wu.spring.orm.integration.compare.infrastructure.persistence;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;

import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import jakarta.annotation.Resource;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.ORMComparisonRepository;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUser;
import org.wu.spring.orm.integration.compare.infrastructure.entity.OrmCompareRecordsDO;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Orm;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Type;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 记录 orm执行过程时间
 */
public abstract class SysUserRepositoryAbstractRecord implements ORMComparisonRepository {

    @Resource
    LazyLambdaStream lazyLambdaStream;

    /**
     * 存储记录
     *
     * @param orm             orm 框架
     * @param lambdaTableType 执行类型
     * @param startTime       开始时间
     * @param endTime         结束时间
     * @param success         执行结果
     */
    public void storyRecord(Orm orm, Type lambdaTableType, LocalDateTime startTime, LocalDateTime endTime, boolean success) {
        storyRecord(orm, lambdaTableType, startTime, endTime, 1, success);
    }

    /**
     * 存储记录
     *
     * @param orm             orm 框架
     * @param lambdaTableType 执行类型
     * @param startTime       开始时间
     * @param endTime         结束时间
     * @param affectsRows     影响行数
     * @param success         执行结果
     */
    public void storyRecord(Orm orm, Type lambdaTableType, LocalDateTime startTime, LocalDateTime endTime, Integer affectsRows, boolean success) {
        OrmCompareRecordsDO ormCompareRecordsDO = new OrmCompareRecordsDO();
        ormCompareRecordsDO.setOrm(orm.name());
        ormCompareRecordsDO.setType(lambdaTableType.name());
        ormCompareRecordsDO.setIsDeleted(false);
        ormCompareRecordsDO.setCreateTime(LocalDateTime.now());
        ormCompareRecordsDO.setUpdateTime(LocalDateTime.now());
        ormCompareRecordsDO.setAffectsRows(affectsRows);
        ormCompareRecordsDO.setSuccess(success);
        // 毫秒
        long durationInMillis = Duration.between(startTime, endTime).toMillis();
        ormCompareRecordsDO.setOperationTime(durationInMillis);
        lazyLambdaStream.upsert(ormCompareRecordsDO);
    }


    /**
     * 重置 测试表
     */
    public void resetTestTableRecords() {
        lazyLambdaStream.delete(LazyWrappers.<SysUserDO>lambdaWrapper().notNull(SysUserDO::getId));
    }

    /**
     * describe 新增
     *
     * @param sysUser 新增
     * @return {@link  Result < SysUser >} 新增后领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
//    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result<SysUser> story(SysUser sysUser) throws Exception {
        return null;
    }

    /**
     * describe 批量新增
     *
     * @param sysUserList 批量新增
     * @return {@link Result< List <SysUser>>} 新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
//    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result<List<SysUser>> batchStory(List<SysUser> sysUserList) throws Exception {
        return null;
    }

    /**
     * describe 查询单个
     *
     * @param sysUser 查询单个
     * @return {@link Result<SysUser>} DTO对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    @Override
    public Result<SysUser> findOne(SysUser sysUser) throws Exception {
        return null;
    }

    /**
     * describe 查询多个
     *
     * @param sysUser 查询多个
     * @return {@link Result<List<SysUser>>} DTO对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    @Override
    public Result<List<SysUser>> findList(SysUser sysUser) throws Exception {
        return null;
    }

    /**
     * describe 分页查询多个
     *
     * @param size    当前页数
     * @param current 当前页
     * @param sysUser 分页查询多个
     * @return {@link Result< LazyPage <SysUser>>} 分页领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    @Override
    public Result<LazyPage<SysUser>> findPage(int size, int current, SysUser sysUser) throws Exception {
        return null;
    }

    /**
     * describe 删除
     *
     * @param sysUser 删除
     * @return {@link Result<SysUser>}
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    @Override
    public Result<SysUser> remove(SysUser sysUser) throws Exception {
        return null;
    }

    /**
     * describe 是否存在
     *
     * @param sysUser 是否存在
     * @return {@link Result<Boolean>} 是否存在
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    @Override
    public Result<Boolean> exists(SysUser sysUser) throws Exception {
        return null;
    }
}
