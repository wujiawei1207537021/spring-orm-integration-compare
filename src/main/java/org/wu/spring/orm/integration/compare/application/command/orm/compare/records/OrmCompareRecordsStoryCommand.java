package org.wu.spring.orm.integration.compare.application.command.orm.compare.records;

import lombok.Data;
import lombok.experimental.Accessors;
import io.swagger.v3.oas.annotations.media.Schema;
import java.lang.Integer;
import java.time.LocalDateTime;
import java.lang.String;
/**
 * describe Orm 操作记录 
 *
 * @author Jia wei Wu
 * @date 2024/02/28 01:02 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyStoryCommand 
 **/
@Data
@Accessors(chain = true)
@Schema(title = "orm_compare_records_story_command",description = "Orm 操作记录")
public class OrmCompareRecordsStoryCommand {


    /**
     * 
     * 影响行数
     */
    @Schema(description ="影响行数",name ="affectsRows",example = "")
    private Integer affectsRows;

    /**
     * 
     * 创建时间
     */
    @Schema(description ="创建时间",name ="createTime",example = "")
    private LocalDateTime createTime;

    /**
     * 
     * 主键
     */
    @Schema(description ="主键",name ="id",example = "")
    private Integer id;

    /**
     * 
     * 是否删除
     */
    @Schema(description ="是否删除",name ="isDeleted",example = "")
    private Boolean isDeleted;

    /**
     * 
     * 执行时间
     */
    @Schema(description ="执行时间",name ="operationTime",example = "")
    private Long operationTime;

    /**
     * 
     * orm(mybatis、lazy、sqltoy)
     */
    @Schema(description ="orm(mybatis、lazy、sqltoy)",name ="orm",example = "")
    private String orm;

    /**
     * 
     * 类型，增加、删除、修改、查询
     */
    @Schema(description ="类型，增加、删除、修改、查询",name ="type",example = "")
    private String type;

    /**
     * 
     * 修改时间
     */
    @Schema(description ="修改时间",name ="updateTime",example = "")
    private LocalDateTime updateTime;

}