package org.wu.spring.orm.integration.compare.infrastructure.persistence.wood;


import jakarta.annotation.Resource;
import net.hasor.dbvisitor.page.Page;
import net.hasor.dbvisitor.page.PageObject;
import org.noear.wood.DataItem;
import org.noear.wood.DbContext;
import org.noear.wood.DbTableQuery;
import org.noear.wood.IPage;
import org.noear.wood.ext.Act2;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUser;
import org.wu.spring.orm.integration.compare.infrastructure.converter.SysUserConverter;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.SysUserRepositoryAbstractRecord;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Orm;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Type;

import java.time.LocalDateTime;
import java.util.List;

/**
 * describe sys_user
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence
 **/
@Component("woodComparisonRepository")
public class WoodComparisonRepository extends SysUserRepositoryAbstractRecord {


    @Resource
    private DbContext dbContext;

    /**
     * describe 新增
     *
     * @param sysUser 新增
     * @return {@link Result<SysUser>} 新增后领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> story(SysUser sysUser) throws Exception {
        boolean success = true;
        super.resetTestTableRecords();
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            // 遇到唯一性索引 用户名、scope
            dbContext.table("sys_user").setEntity(sysUserDO).insert();
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.WOOD, Type.story, startTime, endTime, success);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增
     *
     * @param sysUserList 批量新增
     * @return {@link Result<List<SysUser>>} 新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> batchStory(List<SysUser> sysUserList) throws Exception {
        boolean success = true;
        super.resetTestTableRecords();
        LocalDateTime startTime = LocalDateTime.now();
        try {
            List<SysUserDO> sysUserDOList = sysUserList.stream().map(SysUserConverter.INSTANCE::fromSysUser).toList();

            dbContext.table("sys_user").insertList(sysUserDOList, (sysUserDO, dataItem) -> {
                dataItem.set("id", sysUserDO.getId());
                dataItem.set("username", sysUserDO.getUsername());
                dataItem.set("password", sysUserDO.getPassword());
                dataItem.set("scope", sysUserDO.getScope());
                dataItem.set("status", sysUserDO.getStatus());
                dataItem.set("column_name", sysUserDO.getColumnName());
                dataItem.set("create_time", sysUserDO.getCreateTime());
                dataItem.set("update_time", sysUserDO.getUpdateTime());
                dataItem.set("is_deleted", sysUserDO.getIsDeleted());
                dataItem.set("column_name_1", sysUserDO.getColumnName_1());
                dataItem.set("column_name_2", sysUserDO.getColumnName_2());
                dataItem.set("column_name_3", sysUserDO.getColumnName_3());
                dataItem.set("column_name_4", sysUserDO.getColumnName_4());
                dataItem.set("column_name_5", sysUserDO.getColumnName_5());
                dataItem.set("column_name_6", sysUserDO.getColumnName_6());
                dataItem.set("column_name_7", sysUserDO.getColumnName_7());
                dataItem.set("column_name_8", sysUserDO.getColumnName_8());
                dataItem.set("column_name_9", sysUserDO.getColumnName_9());
                dataItem.set("column_name_10", sysUserDO.getColumnName_10());
                dataItem.set("column_name_11", sysUserDO.getColumnName_11());
                dataItem.set("column_name_12", sysUserDO.getColumnName_12());
                dataItem.set("column_name_13", sysUserDO.getColumnName_13());
                dataItem.set("column_name_14", sysUserDO.getColumnName_14());
                dataItem.set("column_name_15", sysUserDO.getColumnName_15());
                dataItem.set("column_name_16", sysUserDO.getColumnName_16());
                dataItem.set("column_name_17", sysUserDO.getColumnName_17());
                dataItem.set("column_name_18", sysUserDO.getColumnName_18());
                dataItem.set("column_name_19", sysUserDO.getColumnName_19());
                dataItem.set("column_name_20", sysUserDO.getColumnName_20());
                dataItem.set("column_name_21", sysUserDO.getColumnName_21());
                dataItem.set("column_name_22", sysUserDO.getColumnName_22());
                dataItem.set("column_name_23", sysUserDO.getColumnName_23());
                dataItem.set("column_name_24", sysUserDO.getColumnName_24());
                dataItem.set("column_name_25", sysUserDO.getColumnName_25());
                dataItem.set("column_name_26", sysUserDO.getColumnName_26());
                dataItem.set("column_name_27", sysUserDO.getColumnName_27());
                dataItem.set("column_name_28", sysUserDO.getColumnName_28());
                dataItem.set("column_name_29", sysUserDO.getColumnName_29());
                dataItem.set("column_name_30", sysUserDO.getColumnName_30());
                dataItem.set("column_name_31", sysUserDO.getColumnName_31());
                dataItem.set("column_name_32", sysUserDO.getColumnName_32());
                dataItem.set("column_name_33", sysUserDO.getColumnName_33());
                dataItem.set("column_name_34", sysUserDO.getColumnName_34());
                dataItem.set("column_name_35", sysUserDO.getColumnName_35());
                dataItem.set("column_name_36", sysUserDO.getColumnName_36());
                dataItem.set("column_name_37", sysUserDO.getColumnName_37());
                dataItem.set("column_name_38", sysUserDO.getColumnName_38());
                dataItem.set("column_name_39", sysUserDO.getColumnName_39());
                dataItem.set("column_name_40", sysUserDO.getColumnName_40());

            });
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }


        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.WOOD, Type.batchStory, startTime, endTime, sysUserList.size(), success);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个
     *
     * @param sysUser 查询单个
     * @return {@link Result<SysUser>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> findOne(SysUser sysUser) throws Exception {
        boolean success = true;
        super.findOne(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {

            DbTableQuery dbTableQuery = dbContext.table("sys_user");
            if (!ObjectUtils.isEmpty(sysUser.getId())) {
                dbTableQuery
                        .whereEq("id", sysUser.getId());
            }
            SysUser sysUserOne = dbTableQuery.selectItem("column_name, create_time, id, is_deleted, password, scope, status, update_time,username,column_name_1,column_name_2,column_name_3,column_name_4,column_name_5,column_name_6,column_name_7,column_name_8,column_name_9,column_name_10,column_name_11,column_name_12,column_name_13,column_name_14,column_name_15,column_name_16,column_name_17,column_name_18,column_name_19,column_name_20,column_name_21,column_name_22,column_name_23,column_name_24,column_name_25,column_name_26,column_name_27,column_name_28,column_name_29,column_name_30,column_name_31,column_name_32,column_name_33,column_name_34,column_name_35,column_name_36,column_name_37,column_name_38,column_name_39,column_name_40",SysUser.class);
            return ResultFactory.successOf(sysUserOne);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.WOOD, Type.findOne, startTime, endTime, success);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 查询多个
     *
     * @param sysUser 查询多个
     * @return {@link Result<List<SysUser>>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> findList(SysUser sysUser) throws Exception {
        boolean success = true;
        super.findList(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {


            DbTableQuery dbTableQuery = dbContext.table("sys_user");
            if (!ObjectUtils.isEmpty(sysUser.getId())) {
                dbTableQuery
                        .whereEq("id", sysUser.getId());
            }

            List<SysUser> sysUserList =  dbTableQuery
                    .selectList("column_name, create_time, id, is_deleted, password, scope, status, update_time,username,column_name_1,column_name_2,column_name_3,column_name_4,column_name_5,column_name_6,column_name_7,column_name_8,column_name_9,column_name_10,column_name_11,column_name_12,column_name_13,column_name_14,column_name_15,column_name_16,column_name_17,column_name_18,column_name_19,column_name_20,column_name_21,column_name_22,column_name_23,column_name_24,column_name_25,column_name_26,column_name_27,column_name_28,column_name_29,column_name_30,column_name_31,column_name_32,column_name_33,column_name_34,column_name_35,column_name_36,column_name_37,column_name_38,column_name_39,column_name_40",SysUser.class);
            ;
            return ResultFactory.successOf(sysUserList);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.WOOD, Type.findList, startTime, endTime, success);

        }
        return ResultFactory.successOf();

    }

    /**
     * describe 分页查询多个
     *
     * @param size    当前页数
     * @param current 当前页
     * @param sysUser 分页查询多个
     * @return {@link Result<LazyPage<SysUser>>} 分页领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<LazyPage<SysUser>> findPage(int size, int current, SysUser sysUser) throws Exception {
        boolean success = true;
        super.findList(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {

            // 构建分页对象，每页 3 条数据(默认第一页的页码为 0)
            Page pageInfo = new PageObject();
            pageInfo.setPageSize(size);
            pageInfo.setCurrentPage(current - 1);

            // ... 逐一添加


            DbTableQuery dbTableQuery = dbContext.table("sys_user");
            dbTableQuery.paging(current,size);



            if (!ObjectUtils.isEmpty(sysUser.getId())) {
                dbTableQuery
                        .whereEq("id", sysUser.getId());
            }

            if (!ObjectUtils.isEmpty(sysUser.getUsername())) {
                dbTableQuery
                        .whereEq("username", sysUser.getUsername());
            }

            if (!ObjectUtils.isEmpty(sysUser.getIsDeleted())) {
                dbTableQuery
                        .whereEq("is_deleted", sysUser.getIsDeleted());
            }
            IPage<SysUser> records=
                    dbTableQuery
                    .selectPage("column_name, create_time, id, is_deleted, password, scope, status, update_time,username,column_name_1,column_name_2,column_name_3,column_name_4,column_name_5,column_name_6,column_name_7,column_name_8,column_name_9,column_name_10,column_name_11,column_name_12,column_name_13,column_name_14,column_name_15,column_name_16,column_name_17,column_name_18,column_name_19,column_name_20,column_name_21,column_name_22,column_name_23,column_name_24,column_name_25,column_name_26,column_name_27,column_name_28,column_name_29,column_name_30,column_name_31,column_name_32,column_name_33,column_name_34,column_name_35,column_name_36,column_name_37,column_name_38,column_name_39,column_name_40",SysUser.class);
            ;


            LazyPage<SysUser> lazyPage = new LazyPage<>(current, size);
            lazyPage.setRecord(records.getList());
            return ResultFactory.successOf(lazyPage);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.WOOD, Type.findPage, startTime, endTime, size, success);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 删除
     *
     * @param sysUser 删除
     * @return {@link Result<SysUser>}
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> remove(SysUser sysUser) throws Exception {
        boolean success = true;
        super.remove(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);

            DbTableQuery dbTableQuery = dbContext.table("sys_user");
            dbTableQuery.setEntity(sysUserDO).delete();
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.WOOD, Type.remove, startTime, endTime, success);
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在
     *
     * @param sysUser 领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<Boolean> exists(SysUser sysUser) throws Exception {
        boolean success = true;
        super.exists(sysUser);
        boolean exists = false;
        LocalDateTime startTime = LocalDateTime.now();
        try {

            DbTableQuery dbTableQuery = dbContext.table("sys_user");
            if (!ObjectUtils.isEmpty(sysUser.getId())) {
                dbTableQuery
                        .whereEq("id", sysUser.getId());
            }
            exists = dbTableQuery.selectExists();
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }


        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.WOOD, Type.exists, startTime, endTime, success);
        return ResultFactory.successOf(exists);
    }

}