package org.wu.spring.orm.integration.compare.application;


import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.spring.orm.integration.compare.domain.model.orm.compare.records.OrmCompareRecords;
import org.wu.spring.orm.integration.compare.application.command.orm.compare.records.OrmCompareRecordsRemoveCommand;
import org.wu.spring.orm.integration.compare.application.command.orm.compare.records.OrmCompareRecordsStoryCommand;
import org.wu.spring.orm.integration.compare.application.command.orm.compare.records.OrmCompareRecordsUpdateCommand;
import org.wu.spring.orm.integration.compare.application.command.orm.compare.records.OrmCompareRecordsQueryListCommand;
import org.wu.spring.orm.integration.compare.application.command.orm.compare.records.OrmCompareRecordsQueryOneCommand;
import org.wu.spring.orm.integration.compare.application.dto.OrmCompareRecordsDTO;

import java.util.List;

import org.wu.spring.orm.integration.compare.domain.model.orm.compare.records.OrmCompareRecordsEcharts;

/**
 * describe Orm 操作记录
 *
 * @author Jia wei Wu
 * @date 2024/02/28 01:02 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplication
 **/

public interface OrmCompareRecordsApplication {


    /**
     * describe 新增Orm 操作记录
     *
     * @param ormCompareRecordsStoryCommand 新增Orm 操作记录
     * @return {@link Result<OrmCompareRecords>} Orm 操作记录新增后领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    Result<OrmCompareRecords> story(OrmCompareRecordsStoryCommand ormCompareRecordsStoryCommand);

    /**
     * describe 批量新增Orm 操作记录
     *
     * @param ormCompareRecordsStoryCommandList 批量新增Orm 操作记录
     * @return {@link Result<List<OrmCompareRecords>>} Orm 操作记录新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    Result<List<OrmCompareRecords>> batchStory(List<OrmCompareRecordsStoryCommand> ormCompareRecordsStoryCommandList);

    /**
     * describe 更新Orm 操作记录
     *
     * @param ormCompareRecordsUpdateCommand 更新Orm 操作记录
     * @return {@link Result<OrmCompareRecords>} Orm 操作记录领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    Result<OrmCompareRecords> updateOne(OrmCompareRecordsUpdateCommand ormCompareRecordsUpdateCommand);

    /**
     * describe 查询单个Orm 操作记录
     *
     * @param ormCompareRecordsQueryOneCommand 查询单个Orm 操作记录
     * @return {@link Result<OrmCompareRecordsDTO>} Orm 操作记录DTO对象
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    Result<OrmCompareRecordsDTO> findOne(OrmCompareRecordsQueryOneCommand ormCompareRecordsQueryOneCommand);

    /**
     * describe 查询多个Orm 操作记录
     *
     * @param ormCompareRecordsQueryListCommand 查询多个Orm 操作记录
     * @return {@link Result <List<OrmCompareRecordsDTO>>} Orm 操作记录DTO对象
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    Result<List<OrmCompareRecordsDTO>> findList(OrmCompareRecordsQueryListCommand ormCompareRecordsQueryListCommand);

    /**
     * describe 分页查询多个Orm 操作记录
     *
     * @param ormCompareRecordsQueryListCommand 分页查询多个Orm 操作记录
     * @return {@link Result <LazyPage<OrmCompareRecordsDTO>>} 分页Orm 操作记录DTO对象
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    Result<LazyPage<OrmCompareRecordsDTO>> findPage(int size, int current, OrmCompareRecordsQueryListCommand ormCompareRecordsQueryListCommand);

    /**
     * describe 删除Orm 操作记录
     *
     * @param ormCompareRecordsRemoveCommand 删除Orm 操作记录
     * @return {@link Result<OrmCompareRecords>} Orm 操作记录
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    Result<OrmCompareRecords> remove(OrmCompareRecordsRemoveCommand ormCompareRecordsRemoveCommand);

    /**
     * describe 获取echarts数据
     */
    Result<OrmCompareRecordsEcharts> findEchartsData(String type);
}