package org.wu.spring.orm.integration.compare.infrastructure.persistence;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.springframework.stereotype.Repository;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUser;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.ORMComparisonRepository;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUserRepository;

import java.util.List;
/**
 * describe sys_user 
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence 
 **/
@Repository
public class SysUserRepositoryImpl implements  SysUserRepository {

    private final List<ORMComparisonRepository> ormTypeRepositoryList;

    public SysUserRepositoryImpl(List<ORMComparisonRepository> ormTypeRepositoryList) {
        this.ormTypeRepositoryList = ormTypeRepositoryList;
    }

    /**
     * describe 新增
     *
     * @param sysUser 新增
     * @return {@link Result<SysUser>} 新增后领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    @Override
    public Result<SysUser> story(SysUser sysUser) throws Exception {
        for (ORMComparisonRepository ORMComparisonRepository : ormTypeRepositoryList) {
            ORMComparisonRepository.story(sysUser);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增
     *
     * @param sysUserList 批量新增
     * @return {@link Result<List<SysUser>>} 新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    @Override
    public Result<List<SysUser>> batchStory(List<SysUser> sysUserList) throws Exception {
        for (ORMComparisonRepository ormComparisonRepository : ormTypeRepositoryList) {
            ormComparisonRepository.batchStory(sysUserList);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个
     *
     * @param sysUser 查询单个
     * @return {@link Result<SysUser>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    @Override
    public Result<SysUser> findOne(SysUser sysUser) throws Exception {
        for (ORMComparisonRepository ORMComparisonRepository : ormTypeRepositoryList) {
            ORMComparisonRepository.findOne(sysUser);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 查询多个
     *
     * @param sysUser 查询多个
     * @return {@link Result<List<SysUser>>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    @Override
    public Result<List<SysUser>> findList(SysUser sysUser) throws Exception {
        for (ORMComparisonRepository ORMComparisonRepository : ormTypeRepositoryList) {
            ORMComparisonRepository.findList(sysUser);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 分页查询多个
     *
     * @param size    当前页数
     * @param current 当前页
     * @param sysUser 分页查询多个
     * @return {@link Result<LazyPage<SysUser>>} 分页领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    @Override
    public Result<LazyPage<SysUser>> findPage(int size, int current, SysUser sysUser) throws Exception {
        for (ORMComparisonRepository ORMComparisonRepository : ormTypeRepositoryList) {
            ORMComparisonRepository.findPage(size, current, sysUser);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 删除
     *
     * @param sysUser 删除
     * @return {@link Result<SysUser>}
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    @Override
    public Result<SysUser> remove(SysUser sysUser) throws Exception {
        for (ORMComparisonRepository ORMComparisonRepository : ormTypeRepositoryList) {
            ORMComparisonRepository.remove(sysUser);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在
     *
     * @param sysUser 领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    @Override
    public Result<Boolean> exists(SysUser sysUser) throws Exception {
        for (ORMComparisonRepository ORMComparisonRepository : ormTypeRepositoryList) {
            ORMComparisonRepository.exists(sysUser);
        }
        return ResultFactory.successOf();
    }
}