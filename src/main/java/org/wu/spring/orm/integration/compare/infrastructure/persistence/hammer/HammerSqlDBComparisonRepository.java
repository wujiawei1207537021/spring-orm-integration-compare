package org.wu.spring.orm.integration.compare.infrastructure.persistence.hammer;


import cn.featherfly.common.structure.page.PaginationResults;
import cn.featherfly.hammer.dsl.entity.query.EntityQueryFetch;
import cn.featherfly.hammer.expression.entity.query.EntityQueryLimitExecutor;
import cn.featherfly.hammer.sqldb.SqldbHammer;
import jakarta.annotation.Resource;
import org.noear.wood.DbTableQuery;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUser;
import org.wu.spring.orm.integration.compare.infrastructure.converter.SysUserConverter;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.SysUserRepositoryAbstractRecord;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Orm;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Type;

import java.time.LocalDateTime;
import java.util.List;

/**
 * describe sys_user
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence
 **/
@Component("hummerSqlDBComparisonRepository")
public class HammerSqlDBComparisonRepository extends SysUserRepositoryAbstractRecord {


    @Resource
    private SqldbHammer hammer;

    /**
     * describe 新增
     *
     * @param sysUser 新增
     * @return {@link Result<SysUser>} 新增后领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> story(SysUser sysUser) throws  Exception {
        boolean success = true;
        super.resetTestTableRecords();
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            // 遇到唯一性索引 用户名、scope
            hammer.save(sysUserDO);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.HAMMER_SQL_DB, Type.story, startTime, endTime, success);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增
     *
     * @param sysUserList 批量新增
     * @return {@link Result<List<SysUser>>} 新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> batchStory(List<SysUser> sysUserList) throws Exception {
        boolean success = true;
        super.resetTestTableRecords();
        LocalDateTime startTime = LocalDateTime.now();
        try {
            List<SysUserDO> sysUserDOList = sysUserList.stream().map(sysUser -> {
                SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
                sysUserDO.setId(null);
                return sysUserDO;
            }).toList();
//            rs.getLong(index) error 获取主键ID会有问题

            hammer.save(sysUserDOList, 1000);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }


        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.HAMMER_SQL_DB, Type.batchStory, startTime, endTime, sysUserList.size(), success);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个
     *
     * @param sysUser 查询单个
     * @return {@link Result<SysUser>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> findOne(SysUser sysUser) throws Exception {
        boolean success = true;
        super.findOne(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO single = hammer.query(SysUserDO.class).where().eq(SysUserDO::getId, sysUser.getId()).single();
            return ResultFactory.successOf(SysUserConverter.INSTANCE.toSysUser(single));
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.HAMMER_SQL_DB, Type.findOne, startTime, endTime, success);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 查询多个
     *
     * @param sysUser 查询多个
     * @return {@link Result<List<SysUser>>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> findList(SysUser sysUser) throws Exception {
        boolean success = true;
        super.findList(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {
            List<SysUserDO> sysUserDOList = hammer.query(SysUserDO.class).where().eq(SysUserDO::getId, sysUser.getId()).list();
            return ResultFactory.successOf(sysUserDOList.stream().map(SysUserConverter.INSTANCE::toSysUser).toList());
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.HAMMER_SQL_DB, Type.findList, startTime, endTime, success);

        }
        return ResultFactory.successOf();

    }

    /**
     * describe 分页查询多个
     *
     * @param size    当前页数
     * @param current 当前页
     * @param sysUser 分页查询多个
     * @return {@link Result<LazyPage<SysUser>>} 分页领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<LazyPage<SysUser>> findPage(int size, int current, SysUser sysUser) throws Exception {
        boolean success = true;
        super.findList(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {
            LazyPage<SysUser> lazyPage = new LazyPage<>(current, size);
            PaginationResults<SysUserDO> pagination = hammer.query(SysUserDO.class)
                    .where()
                    .eq(SysUserDO::getId, sysUser.getId())
                    .and().eq(SysUserDO::getUsername, sysUser.getUsername())
                    .and().eq(SysUserDO::getIsDeleted, sysUser.getIsDeleted())
                    .limit(current, size)
                    .pagination();
            lazyPage.setTotal(pagination.getTotal());
            lazyPage.setRecord(pagination.getPageResults().stream().map(SysUserConverter.INSTANCE::toSysUser).toList());
            return ResultFactory.successOf(lazyPage);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.HAMMER_SQL_DB, Type.findPage, startTime, endTime, size, success);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 删除
     *
     * @param sysUser 删除
     * @return {@link Result<SysUser>}
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> remove(SysUser sysUser) throws Exception {
        boolean success = true;
        super.remove(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            hammer.delete(sysUserDO);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.HAMMER_SQL_DB, Type.remove, startTime, endTime, success);
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在
     *
     * @param sysUser 领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<Boolean> exists(SysUser sysUser) throws Exception {
        boolean success = true;
        super.exists(sysUser);
        boolean exists = false;
        LocalDateTime startTime = LocalDateTime.now();
        try {
            long count = hammer.query(SysUserDO.class).where().eq(SysUserDO::getId, sysUser.getId()).count();
            exists = count!=0;
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }


        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.HAMMER_SQL_DB, Type.exists, startTime, endTime, success);
        return ResultFactory.successOf(exists);
    }

}