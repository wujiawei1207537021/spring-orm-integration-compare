package org.wu.spring.orm.integration.compare.application.assembler;

import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUser;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserRemoveCommand;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserStoryCommand;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserUpdateCommand;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserQueryListCommand;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserQueryOneCommand;
import org.wu.spring.orm.integration.compare.application.dto.SysUserDTO;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;
/**
 * describe sys_user 
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyAssembler 
 **/
@Mapper
public interface SysUserDTOAssembler {


    /**
     * describe MapStruct 创建的代理对象
     *
     
     
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    SysUserDTOAssembler INSTANCE = Mappers.getMapper(SysUserDTOAssembler.class);
    /**
     * describe 应用层存储入参转换成 领域对象
     *
     * @param sysUserStoryCommand 保存对象     
     * @return {@link SysUser} 领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
     SysUser toSysUser(SysUserStoryCommand sysUserStoryCommand);
    /**
     * describe 应用层更新入参转换成 领域对象
     *
     * @param sysUserUpdateCommand 更新对象     
     * @return {@link SysUser} 领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
     SysUser toSysUser(SysUserUpdateCommand sysUserUpdateCommand);
    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param sysUserQueryOneCommand 查询单个对象参数     
     * @return {@link SysUser} 领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
     SysUser toSysUser(SysUserQueryOneCommand sysUserQueryOneCommand);
    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param sysUserQueryListCommand 查询集合对象参数     
     * @return {@link SysUser} 领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
     SysUser toSysUser(SysUserQueryListCommand sysUserQueryListCommand);
    /**
     * describe 应用层删除入参转换成 领域对象
     *
     * @param sysUserRemoveCommand 删除对象参数     
     * @return {@link SysUser} 领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
     SysUser toSysUser(SysUserRemoveCommand sysUserRemoveCommand);
    /**
     * describe 持久层领域对象转换成DTO对象
     *
     * @param sysUser 领域对象     
     * @return {@link SysUserDTO} DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
     SysUserDTO fromSysUser(SysUser sysUser);
}