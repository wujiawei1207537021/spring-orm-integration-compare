package org.wu.spring.orm.integration.compare.infrastructure.converter;

import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUser;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;
/**
 * describe sys_user 
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureConverter 
 **/
@Mapper
public interface SysUserConverter {


    /**
     * describe MapStruct 创建的代理对象
     *
     
     
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    SysUserConverter INSTANCE = Mappers.getMapper(SysUserConverter.class);
    /**
     * describe 实体对象 转换成领域对象
     *
     * @param sysUserDO 实体对象     
     * @return {@link SysUser} 领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    SysUser toSysUser(SysUserDO sysUserDO);
    /**
     * describe 领域对象 转换成实体对象
     *
     * @param sysUser 领域对象     
     * @return {@link SysUserDO} 实体对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
     SysUserDO fromSysUser(SysUser sysUser); 
}