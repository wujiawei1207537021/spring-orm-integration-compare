package org.wu.spring.orm.integration.compare;

public class GenerateField {
    public static void main(String[] args) {
        /**
         * 创建do对象增加40个字段
         */
        for (int i = 1; i <= 40; i++) {
            System.out.println(String.format("/**\n" +
                            "     * 额外字段%s\n" +
                            "     */\n" +
                            "    @Schema(description = \"额外字段%s\", name = \"columnName%s\", example = \"\")\n" +
                            "    @LazyTableField(name = \"column_name_%s\", comment = \"额外字段%s\", columnType = \"varchar(255)\")\n" +
                            "    @Column(name = \"column_name_%s\", comment = \"额外字段%s\")// sqltoy\n" +
                            "    @jakarta.persistence.Column(name = \"column_name_%s\", columnDefinition = \"额外字段%s\")// jpa\n" +
                            "    @org.beetl.sql.annotation.entity.Column(value = \"column_name_%s\")// beetlsql\n" +
                            "    @net.hasor.dbvisitor.mapping.Column(name = \"column_name_%s\") // db_visitor\n" +
                            "    @com.dream.system.annotation.Column(\"column_name_%s\") // dream_orm\n" +
                            "    @javax.persistence.Column(name = \"column_name_%s\") //hammer_sql\n" +
                            "    private String columnName_%s;",
                    i, i, i, i, i, i, i, i, i, i, i, i, i,i

            ));
        }

        /**
         * 创建dto对象增加40个字段
         */
//        for (int i = 1; i <=40; i++) {
//            System.out.println(String.format("  /**\n" +
//                    "     * \n" +
//                    "     * 额外字段%s\n" +
//                    "     */\n" +
//                    "    @Schema(description =\"额外字段%s\",name =\"columnName%s\",example = \"\")\n" +
//                    "    private String columnName_%s;",
//                    i,i,i,i
//                    ));
//        }

        /**
         * sql 增加40个字段
         */
//        for (int i = 1; i <= 40; i++) {
////            System.out.printf("`column_name_%s`,", i);
//            System.out.printf("#{item.columnName_%s},", i);
//        }
//        System.out.println();
//        for (int i = 1; i <= 40; i++) {
//            System.out.printf(String.format("column_name_%s,",i));
//        }
    }
}
