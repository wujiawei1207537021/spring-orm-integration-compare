

### orm框架使用性能比较
### 测试方法拉取代码，配置数据库账号信息，直接启动，然后打开控制台页面通过页面点击测试


#### 比较mybatis-plus、lazy、sqltoy、mybatis-flex、easy-query、mybatis-mp、jpa、dbvisitor、beetlsql、dream_orm、wood、hammer_sql_db、原生jdbc、qdbc 操作数据

#### 环境：

    idea 
    jdk17
    spring boot 3.0.7
    mysql 8.0

### 测试条件常规对象

| orm 框架        | 是否支持xml | 是否支持 Lambda | 对比版本                 | 编码方式                         | 注意事项                           |
|---------------|---------|-------------|----------------------|------------------------------|--------------------------------|
| mybatis       | ☑️      | ☑️          | 3.5.4                | lambda +xml 优化               | ------------------------------ |
| sqltoy        | ☑️      | ☑️          | 5.2.98               | lambda                       | ------------------------------ |
| lazy          | ✖️      | ☑️          | 1.2.4-JDK17-SNAPSHOT | lambda                       | ------------------------------ |
| mybatis-flex  | ☑️      | ☑️          | 1.8.0                | lambda +xml 优化               | ------------------------------ |
| easy-query    | ✖️      | ☑️          | 1.10.31              | lambda                       | ------------------------------ |
| mybatis-mp    | ☑️      | ☑️          | 1.4.1                | xml 优化                       | ------------------------------ |
| jpa           | ☑️      | ☑️          | 3.0.7                | ----------------------       | ------------------------------ |
| dbvisitor     | ☑️      | ☑️          | 5.4.3                | xml 优化                       | ------------------------------ |
| beetlsql      | 支持md    | ☑️          | 3.26.0-RELEASE       | insert ignore into 优化        | ------------------------------ |
| dream_orm     | ✖️      | ☑️          | 1.3.0                | insert ignore into (当前版本不支持) | ------------------------------ |
| wood          | ☑️      | ☑️          | 1.2.9                | insert ignore into (当前版本不支持) | ------------------------------ |
| hammer_sql_db | ☑️      | ☑️          | 0.7.0                | insert ignore into (当前版本不支持) | 连接的数据库中如果没有表项目无法启动             |
| jdbc          | ✖️      | ✖️          |                      | ----------------------       | ------------------------------ |
| qdbc          | ☑️      | ☑️          | 4.2.7                | ----------------------       | ------------------------------ |

### 数据库表(含有唯一性索引s_u)

```sql
CREATE TABLE `sys_user`
(
    `column_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '额外字段',
    `create_time` datetime                                DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
    `id`          bigint NOT NULL AUTO_INCREMENT COMMENT '用户ID',
    `is_deleted`  tinyint(1) DEFAULT NULL COMMENT 'null',
    `password`    varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '密码',
    `scope`       varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'null',
    `status`      tinyint(1) DEFAULT NULL COMMENT '状态',
    `update_time` datetime                                DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `username`    varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户名',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `s_u` (`scope`,`username`)
) ENGINE=InnoDB AUTO_INCREMENT=9223371632070323791 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
```

### 比较方法:增加、修改、删除、分页查询（当前项目暂时只比较批量新增和分页）

#### 项目设计

- 声明 ORMRepository 接口提供对应增删改查方法
  ![ORMRepository.png](ORMRepository.png)

- 声明 ORMComparisonRepository接口 继承 ORMRepository 下游由不同ORM实现
- 声明 SysUserRepository 接口 继承 ORMRepository 用于循环调用不同orm实现方法执行方法测试产生测试结果


- 声明抽象类 SysUserRepositoryAbstractRecord 继承 ORMComparisonRepository 并且提供对应的框架执行结果存储
  ![SysUserRepositoryAbstractRecord.png](SysUserRepositoryAbstractRecord.png)

- 不同ORM框架mybatis-plus、sqltoy、Lazy、easy-query、mybatis-mp、jpa、dbvisitor、beetlsql、dream_orm、wood、hammer_sql_db、原生jdbc、qdbc 创建 ORMComparisonRepository 的实现
- ![Repository.png](Repository.png)

- 不同 ORM 操作数据的实现

![batchStory_calls.png](batchStory_calls.png)

### 测试条件 批量插入数据 10、100、1000、10000、100000 ，分页查询数据 10、100、1000、10000、100000

    项目启动后使用浏览器打开 http://localhost:1003/sys/user/run-compare
![Index.png](Index.png)
### 测试条件（细节比较） 批量插入数据 1～10000，分页查询数据 1～10000

    项目启动后使用浏览器打开 http://localhost:1003/sys/user/run-particulars-compare

### 导出测试数据为MD

    项目启动后使用浏览器打开 http://localhost:1003/sys/user/export-compare-result

### 测试执行过程

    清空需要插入表中所有数据
    通过ORM框架进行数据批量新增、而后进行分页查询，记录消耗时间，输出md文档

### [查看结果曲线图](http://localhost:1003/)

![result_line.png](result_line.png)

### 测试结果（结果只提供参考）


| MYBATIS_FLEX(batchStory) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|--------------------------|----------|----------|-----------|------------|-------------|
| 执行时间:                    | **93毫秒** | **55毫秒** | **525毫秒** | **3961毫秒** | **38358毫秒** |

| WOOD(batchStory) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|------------------|----------|----------|-----------|------------|-------------|
| 执行时间:            | **17毫秒** | **28毫秒** | **208毫秒** | **1320毫秒** | **22854毫秒** |

| MYBATIS_MP(batchStory) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|------------------------|----------|----------|-----------|------------|-------------|
| 执行时间:                  | **20毫秒** | **86毫秒** | **388毫秒** | **3323毫秒** | **33831毫秒** |

| MYBATIS_PLUS(batchStory) | 影响行数:10  | 影响行数:100  | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|--------------------------|----------|-----------|-----------|------------|-------------|
| 执行时间:                    | **51毫秒** | **135毫秒** | **350毫秒** | **3317毫秒** | **33736毫秒** |

| SQLTOY(batchStory) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|--------------------|----------|----------|-----------|------------|-------------|
| 执行时间:              | **39毫秒** | **33毫秒** | **581毫秒** | **1513毫秒** | **32076毫秒** |

| QDBC(batchStory) | 影响行数:10   | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|------------------|-----------|----------|-----------|------------|-------------|
| 执行时间:            | **164毫秒** | **90毫秒** | **909毫秒** | **6746毫秒** | **71587毫秒** |

| LAZY(batchStory) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|------------------|----------|----------|-----------|------------|-------------|
| 执行时间:            | **13毫秒** | **29毫秒** | **260毫秒** | **1711毫秒** | **20745毫秒** |

| JDBC(batchStory) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|------------------|----------|----------|-----------|------------|-------------|
| 执行时间:            | **49毫秒** | **13毫秒** | **69毫秒**  | **615毫秒**  | **6456毫秒**  |

| DB_VISITOR(batchStory) | 影响行数:10   | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|------------------------|-----------|----------|-----------|------------|-------------|
| 执行时间:                  | **229毫秒** | **24毫秒** | **216毫秒** | **1458毫秒** | **17238毫秒** |

| JPA(batchStory) | 影响行数:10   | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|-----------------|-----------|----------|-----------|------------|-------------|
| 执行时间:           | **320毫秒** | **45毫秒** | **284毫秒** | **1902毫秒** | **21815毫秒** |

| EASY_QUERY(batchStory) | 影响行数:10   | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|------------------------|-----------|----------|-----------|------------|-------------|
| 执行时间:                  | **110毫秒** | **35毫秒** | **429毫秒** | **2722毫秒** | **20608毫秒** |

| HAMMER_SQL_DB(batchStory) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|---------------------------|----------|----------|-----------|------------|-------------|
| 执行时间:                     | **81毫秒** | **19毫秒** | **326毫秒** | **2413毫秒** | **24478毫秒** |

| DREAM_ORM(batchStory) | 影响行数:10   | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|-----------------------|-----------|----------|-----------|------------|-------------|
| 执行时间:                 | **108毫秒** | **25毫秒** | **216毫秒** | **2628毫秒** | **18042毫秒** |

| BEETL_SQL(batchStory) | 影响行数:10   | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|-----------------------|-----------|----------|-----------|------------|-------------|
| 执行时间:                 | **213毫秒** | **59毫秒** | **263毫秒** | **1904毫秒** | **18678毫秒** |

| MYBATIS_FLEX(findPage) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|------------------------|----------|----------|-----------|------------|-------------|
| 执行时间:                  | **39毫秒** | **16毫秒** | **86毫秒**  | **594毫秒**  | **7753毫秒**  |

| WOOD(findPage) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|----------------|----------|----------|-----------|------------|-------------|
| 执行时间:          | **17毫秒** | **17毫秒** | **51毫秒**  | **505毫秒**  | **6735毫秒**  |

| MYBATIS_MP(findPage) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|----------------------|----------|----------|-----------|------------|-------------|
| 执行时间:                | **35毫秒** | **15毫秒** | **72毫秒**  | **499毫秒**  | **7552毫秒**  |

| MYBATIS_PLUS(findPage) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|------------------------|----------|----------|-----------|------------|-------------|
| 执行时间:                  | **34毫秒** | **12毫秒** | **66毫秒**  | **577毫秒**  | **6949毫秒**  |

| SQLTOY(findPage) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|------------------|----------|----------|-----------|------------|-------------|
| 执行时间:            | **21毫秒** | **10毫秒** | **30毫秒**  | **298毫秒**  | **4161毫秒**  |

| QDBC(findPage) | 影响行数:10  | 影响行数:100  | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|----------------|----------|-----------|-----------|------------|-------------|
| 执行时间:          | **35毫秒** | **104毫秒** | **945毫秒** | **9243毫秒** | **96091毫秒** |

| LAZY(findPage) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|----------------|----------|----------|-----------|------------|-------------|
| 执行时间:          | **17毫秒** | **16毫秒** | **45毫秒**  | **389毫秒**  | **4783毫秒**  |

| JDBC(findPage) | 影响行数:10 | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|----------------|---------|----------|-----------|------------|-------------|
| 执行时间:          | **4毫秒** | **8毫秒**  | **52毫秒**  | **428毫秒**  | **5458毫秒**  |

| DB_VISITOR(findPage) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|----------------------|----------|----------|-----------|------------|-------------|
| 执行时间:                | **17毫秒** | **18毫秒** | **42毫秒**  | **455毫秒**  | **5939毫秒**  |

| JPA(findPage) | 影响行数:10   | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|---------------|-----------|----------|-----------|------------|-------------|
| 执行时间:         | **136毫秒** | **23毫秒** | **63毫秒**  | **454毫秒**  | **5402毫秒**  |

| EASY_QUERY(findPage) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|----------------------|----------|----------|-----------|------------|-------------|
| 执行时间:                | **60毫秒** | **9毫秒**  | **40毫秒**  | **328毫秒**  | **3984毫秒**  |

| HAMMER_SQL_DB(findPage) | 影响行数:10   | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|-------------------------|-----------|----------|-----------|------------|-------------|
| 执行时间:                   | **121毫秒** | **15毫秒** | **63毫秒**  | **623毫秒**  | **7410毫秒**  |

| DREAM_ORM(findPage) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|---------------------|----------|----------|-----------|------------|-------------|
| 执行时间:               | **62毫秒** | **11毫秒** | **32毫秒**  | **320毫秒**  | **4097毫秒**  |

| BEETL_SQL(findPage) | 影响行数:10  | 影响行数:100 | 影响行数:1000 | 影响行数:10000 | 影响行数:100000 |
|---------------------|----------|----------|-----------|------------|-------------|
| 执行时间:               | **44毫秒** | **13毫秒** | **51毫秒**  | **432毫秒**  | **5670毫秒**  |









#### 写在最后
经过不间断时间的框架收集、学习、实验、编码、测试市面上常见的ORM框架，过程中拜读了很多作者的博文、样例，学习很多收获很多。
重新梳理下整理的框架：mybatis-plus、lazy、sqltoy、mybatis-flex、easy-query、mybatis-mp、jpa、dbvisitor、beetlsql、dream_rom、wood、hammer_sql_db、原生jdbc、qdbc

##### 下面从一下几点出发作出总结
- 文档方面：学习过程中mybatis-plus、jpa、db_visitor提供的文档资料是比较全和完善，经得住市场的考验
- 技术方面：beetlsql、easy-query、mybatis、wood、db_visitor系列 三类框架都已经支持spring 和solon生态 其技术架构设计可以推荐大家学习
- 并发方面：jpa、db_visitor 还需要开发时候深度优化处理
- 大数据存储方面： Lazy 、原生jdbc具有一定优势
- 大数据查询方面：sqltoy、dream_orm、Easy_query、lazy、db_visitor 反射处理的比较优秀

####  小数据量下各ORM框架处理时间大体相近或者通过优化后趋于一致，重点看一万数据以后框架处理能力体现价值


以上是个人整理的观点，如果大家有不同的想法和意见可以在[gitee](https://gitee.com/wujiawei1207537021)或者个人博客留言[CSDN](https://blog.csdn.net/qq_22903677?type=blog)




###### 细节数据对比（一万以内基本相差不大）
- 细节数据对比，数据属于并发行测试数据，如果测试总数是一百，那么会执行一百次batchStory，一百次findPage 每次执行的条数在之前数据的基础上+1
###### 从形成的折线图看（具体趋势看排名与测试结果）
- 存储性能对比: lazy、原生jdbc、mybatis-flex、mybatis-mp、mybatis-plus、dream_rom、sqltoy、beetlSql、hammer_sql_db、db_visitor、easy-query、qdbc 更适合并发性数据存储。jpa 处理耗时较长波动较大
- 分页查询性能对比: lazy、mybatis-flex、mybatis-mp、mybatis-plus、 dream_rom、easy-query、sqltoy、db_visitor、beetlSql 、jpa、hammer_sql_db、原生jdbc都比较稳定。qdbc性能不佳

![particularsBatchStory.png](particularsBatchStory.png)
![particularsFindPage.png](particularsFindPage.png)

##### 批量保存：

- 一万条数据以内  原生jdbc、lazy、mybatis-flex、mybatis-mp、mybatis-plus、easy-query、sqltoy、beetlSql、jpa、db_visitor、wood、hammer_sql_db、qdbc 性能趋于一致
- 十万数据时，处理时间由快到慢依次是:
  - 两万毫秒以内:原生jdbc、sqltoy、db_visitor、wood、beetl_sql、dream_rom、easy-query、lazy、hammer_sql_db、
  - 两万毫秒以上: jpa、mybatis-plus、mybatis-mp、mybatis-flex、qdbc

##### 分页查询：

- 一万条数据以内 几款ORM均保持在600毫秒内
- 十万数据时，处理时间由快到慢依次是:
  - 四千毫秒以内：sqltoy、dream_rom、lazy、easy-query、beetlSql、jpa、db_visitor
  - 四千毫秒以上：原生jdbc、mybatis-mp、mybatis-plus、wood、mybatis-flex、hammer_sql_db、qdbc

### 快速数据对比(大数据曲线图)

![batchStory.png](batchStory.png)

![findPage.png](findPage.png)


###### 优化项 
- 时间 2024年5月7日
  - 添加 wood框架对比测试数据库存储和查询
  - 测试结果容易上手快速集成，但是内部使用了大量字符串不易于编写
- 时间 2024年5月8日
  - 针对mysql 配置 rewriteBatchedStatements=true 保存时间明显提高
- 时间：2024年5月9日
  - 事件 新增hammer_sql_db框架对比测试，该框架上手比较简单比较灵活
  - 测试结果hammer_sql_db 批量存储做的很优秀，但当分页获取数据量超过一万后分页查询性能开始下降
- 时间：2024年5月11日
  - 事件 新增原生jdbc测试
  - 测试结果 因为未对jdbc过分优化，从结果过上看insertBatch 存在一定优势但是不大、分页查询结果集映射单纯for循环迭代会很慢
- 时间：2024年5月20日
  - 事件 优化easy-query批量新增
  - 测试结果 优化后easy-query 批量新增数据趋于稳定
- 时间：2024年5月21日
  - 事件 优化jpa数据存储、十万数据存储操作分批处理，表结构新增四十个测试字段
  - 测试结果 优化后jpa 批量新增数据趋于稳定事件大大缩减，新增四十个字段后所有ORM框架处理时间明显增多，但不是呈现倍数增加
- 时间：2024年5月28日
  - 事件 新增qdbc 框架对比
  - 测试结果 qdbc基于spring JdbcTemplate处理存储和查询性能均有所不佳
##### issues 处理
- [#I9Q8UD](https://gitee.com/wujiawei1207537021/spring-orm-integration-compare/issues/I9Q8UD)
  - 事件：大表大数据处理，在原先表结构上增加40个字段
  - 测试结果：当表结构修改时使用原生sql需要修改(db_visitor、原生jdbc、mybatis、mybatis-flex、mybatis-mp),相反使用lambda表达式则不需要过多调整
  - 问题：
    - 如果表不存在hammer框架回无法启动，已经提交开发者issues
    - 使用过程中如果字段中包含下划线那么hammer 框架必须声明字段框架否则数据无法插入影响测试结果，对应的issues已经提交
- [#I9QGIL](https://gitee.com/wujiawei1207537021/spring-orm-integration-compare/issues/I9QGIL)
  - 事件：easy-query 优化测试结果
- [#I9QDC7](https://gitee.com/wujiawei1207537021/spring-orm-integration-compare/issues/I9QDC7)
  - 事件： 新增结果导出Excel
- [#I9P24G](https://gitee.com/wujiawei1207537021/spring-orm-integration-compare/issues/I9P24G)
  - 优化纯jdbc测试
- [#I9SQ6K](https://gitee.com/wujiawei1207537021/spring-orm-integration-compare/issues/I9SQ6K)
  - 优化hammer框架连接数据库检测数据库空表问题

### 关注公众号第一时间获取动态
![WeChat.png](WeChat.png)
#### [当前项目地址](https://gitee.com/wujiawei1207537021/spring-orm-integration-compare):https://gitee.com/wujiawei1207537021/spring-orm-integration-compare

#### [lazy-orm地址](https://gitee.com/wujiawei1207537021/wu-framework-parent/tree/master/wu-database-parent):https://gitee.com/wujiawei1207537021/wu-framework-parent/tree/master/wu-database-parent

#### [mybatis地址](https://gitee.com/baomidou/mybatis-plus):https://gitee.com/baomidou/mybatis-plus

#### [sqltoy地址](https://gitee.com/sagacity/sagacity-sqltoy):https://gitee.com/sagacity/sagacity-sqltoy

#### [mybatis-flex地址](https://gitee.com/mybatis-flex/mybatis-flex):https://gitee.com/mybatis-flex/mybatis-flex

#### [easy-query地址](https://gitee.com/xuejm/easy-query):https://gitee.com/xuejm/easy-query

#### [mybatis-mp地址](https://gitee.com/mybatis-mp/mybatis-mp):https://gitee.com/mybatis-mp/mybatis-mp

#### [dbvisitor地址](https://gitee.com/zycgit/dbvisitor):https://gitee.com/zycgit/dbvisitor

#### [beetlsql地址](https://gitee.com/xiandafu/beetlsql):https://gitee.com/xiandafu/beetlsql

#### [dream-orm地址](https://gitee.com/moxiaoai/dream-orm):https://gitee.com/moxiaoai/dream-orm

#### [wood地址](https://gitee.com/noear/wood):https://gitee.com/noear/wood

#### [hammer_sql_db地址](https://gitee.com/featherfly/hammer):https://gitee.com/featherfly/hammer

#### [qdbc地址](https://gitee.com/qdbp/qdbp-jdbc):https://gitee.com/qdbp/qdbp-jdbc




